# Issue
When a `sub-asset` script is deleted or modified so that it's no longer a `ScriptableObject` script, there is no way to get a reference of the `Object` to be able to delete it. The available functions to load the assets at the `main asset` path will return null for the missing script `sub-asset`. While you can manually delete the .asset file on a text editor, you need to understand what you are doing or you could end up with a corrupt .asset file. The provided function will allow you to do it within the `Unity Editor`.

# How does it work?
It doesn't use any internal function to delete the `sub-asset`. Instead it uses some simple logic to do the .asset file modification for you. The following is a quick rundown of how it works:

1. It creates a copy of the `main asset` with the corrupt `sub-asset` using `UnityEditor` functions.
2. Reparents the `sub-assets` to the new clone copy.
3. Finally it modifies the .asset file from within the OS by deleting the original one (with the corrupt `sub-asset`) and renaming the copy to be the same as the original. Note that this works because the corrupt `sub-asset` is not copied over.

# How to use it?
Just copy the function located at <a href="https://gitlab.com/RotaryHeart-UnityShare/subassetmissingscriptdelete/blob/master/FixMissingScript">FixMissingScript</a> file and paste it into any `Editor` script. Note that it's important that the function is inside an `Editor` script since it uses `UnityEditor`.